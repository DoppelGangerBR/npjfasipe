<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Acao extends Model
{
    protected $fillable = ['acao','descricao'];
    protected $datas = ['created_at','updated_at','deleted_at'];
    protected $table = 'acoes';

    protected function Acao(){
        return $this->hasMany('App\Acao');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pessoa;
class PessoaController extends Controller
{
    public function index(){
        $pessoa = Pessoa::all();
        return response()->json($pessoa);
    }
    public function store(Request $request){
        $dados = $request->all();
        try{
            $pessoa = Pessoa::create($dados);
            if($pessoa){
                return response()->json(['status'=>true,'msg'=>'Cadastro realizado com sucesso!']);
            }else{
                return response()->json(['status'=>false,'msg'=>'Ocorreu uma falha, por favor, tente novamente!']);
            }
        }catch(\SqlException $e){
            return response()->json(['status'=>false,'msg'=>$e]);
        }
    }
    public function desativaPessoa(Request $request){
        $idPessoaDesativada = $request->header('idpessoa');
        $pessoa = Pessoa::where('id','=',$idPessoaDesativada)->update(['status'=> 0]);
        if($pessoa){
            return response()->json(['msg'=>'Usuário desativado com sucesso!','status'=>true]);
        }else{
            return response()->json(['msg'=>'Falha ao desativar usuário!','status'=>false]);
        }
    }

    public function updatePessoa(Request $request){
        $dadosAlteracao = $request->except('id');
        $id = $request->only('id');
        $pessoa = Pessoa::where('id','=',$id)->update($dadosAlteracao);
        if($pessoa){
            return response()->json(['msg'=>'Pessoa alterada com sucesso!','status'=>true]);
        }else{
            return response()->json(['msg'=>'Falha ao alterar Pessoa!','status'=>false]);
        }
    }

    public function retornaEspecifico(Request $request){
        $tipoRetorno = $request->header('tipo');
        $pessoa = Pessoa::where([['tipopessoa','=',$tipoRetorno],['status','=',1]])->get();
        return response()->json($pessoa);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
class AuthController extends Controller
{
    public function login(Request $request){
        $dados = $request->all();
        try{
            $Usuario = Usuario::where('usuario','=',$dados['usuario'])->first();
            if(!$Usuario){
                return response()->json(['status'=>false, 'erro'=>'Usuário não encontrado']);
            }
            if(!\Hash::check($dados['senha'], $Usuario->senha)){
                return response()->json(['status'=>false, 'erro'=>'Senha incorreta']);
            }
            if($Usuario->ativo == 0){                
                return response()->json(['status'=>false, 'erro'=>'Usuário desativado']);                
            }
            $token = JWTAuth::fromUser($Usuario);
            $objectToken = JWTAuth::setToken($token);
            $Usuario->auth_token = $token;
            $Usuario->save();
            $expiracao = JWTAuth::decode($objectToken->getToken())->get('exp');
            return response()->json(['status'=>true,'token'=>$token]);
        }catch(\SqlException $e){
            return $e;
        }
    }

    public function ValidaToken(Request $request){
        $user_ret = false;
        $dados = $request->token;
        $usuario = "";
        if(!$dados == null || !$dados == ''){
            $usuario = Usuario::where('auth_token', $request->token)->get()->first();
            if($usuario != null){
                $user_ret = true;                
            }
        }
        return response()->json(['Autenticado'=>$user_ret]);
    }
    public function VerificaSeEAdministrador(Request $request){
        $dados = $request->only(['token']);               
        $Administrador = false; 
        if(!$dados == null or !$dados == ''){
            $where = ['auth_token' => $dados['token']];                
            $Administrador = Usuario::select('administrador')->where($where)->pluck('administrador');             
            $Administrador =  str_replace("[","",$Administrador);
            $Administrador = str_replace("]","", $Administrador);
        }
        return $Administrador;
    }

    public function getAuthenticatedUser(){
        try {
            if (!$Usuario = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        }catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        return response()->json(compact('user'));
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Usuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('usuario',50)->unique();
            $table->string('email')->nullable();
            $table->string('senha', 450);
            $table->string('auth_token', 450)->nullable();
            $table->integer('ativo')->default(1);
            $table->boolean('administrador')->default(false);
            $table->boolean('podealterar')->default(false);
            $table->boolean('podeincluir')->default(false);
            $table->boolean('podeexcluir')->default(false);
            $table->timestamps();
        });
        $senha = \Hash::make('Fasipe@16');
        
        DB::Statement(
            'INSERT INTO usuarios(usuario,email,senha,ativo,administrador,podealterar,podeincluir) VALUES(\'administrador\',\'dev1@fasipe.com.br\', \''.$senha.'\', 1,1,1,1)'
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}

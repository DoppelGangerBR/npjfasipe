<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriaTabelaProcessoXPartes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('processoxparte', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });
        //É preciso criar manualmente os campos por que o MySql parece não se dar bem com o migrate do laravel
        DB::Statement(
            'alter table processoxparte add COLUMN codparte INT UNSIGNED NOT NULL;'
        );
        DB::Statement(
            'alter table processoxparte add COLUMN codprocesso INT UNSIGNED NOT NULL;'
        );
        DB::Statement(
            'ALTER TABLE processoxparte ADD CONSTRAINT fk_codparte FOREIGN KEY (codparte) REFERENCES pessoas(id);'
        );
        DB::Statement(
            'ALTER TABLE processoxparte ADD CONSTRAINT fk_codprocesso FOREIGN KEY (codprocesso) REFERENCES processos(id);'
        );
        //Tive que criar esses dois campos da tabela pessoas nessa classe para garantir que quando esse campos forem criados
        //A tabela pessoa já tenha sido criada também (Esse script é o ultimo a rodar no migrate)
        DB::Statement(
            'alter table pessoas add COLUMN ra INT(12) UNSIGNED;'
        );
        DB::Statement(
            'alter table pessoas add COLUMN idade INT(3) UNSIGNED;'
        );
        DB::Statement(
            'alter table acoes add column status SMALLINT(1) default 1'
        );
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('processoxparte');
    }
}

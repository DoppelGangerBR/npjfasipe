<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Processo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('processos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numeroprocesso',50)->unique();
            $table->string('observacao',1000);
            $table->smallInteger('status')->default(1); //0 = Excluido, 1 = Ativo, 2 = Passivo
            $table->timestamps();            
            $table->softDeletes();
        });
        DB::Statement(
            'alter table processos add COLUMN codaluno INT UNSIGNED NOT NULL;'
        );
        DB::Statement(
            'alter table processos add COLUMN codacao INT UNSIGNED NOT NULL;'
        );
        DB::Statement(
            'alter table processos add COLUMN codprofessor INT UNSIGNED NOT NULL;'
        );
        DB::Statement(
            'ALTER TABLE processos ADD CONSTRAINT fk_codaluno FOREIGN KEY (codaluno) REFERENCES pessoas(id);'
        );
        DB::Statement(
            'ALTER TABLE processos ADD CONSTRAINT fk_codprofessor FOREIGN KEY (codprofessor) REFERENCES pessoas(id);'
        );
        DB::Statement(
            'ALTER TABLE processos ADD CONSTRAINT fk_codacao FOREIGN KEY (codacao) REFERENCES acoes(id);'
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('processos');
    }
}

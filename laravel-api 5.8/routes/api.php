<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('auth/login', 'AuthController@login');
Route::post('auth/', 'AuthController@ValidaToken');
Route::post('authadm', 'AuthController@VerificaSeEAdministrador');

Route::get('processo/especifico', 'ProcessoController@retornaProcessos');
Route::group(['middleware'=>['jwt.verify']], function(){
    //Rotas de usuario
    Route::post('usuario/permissoes','UsuarioController@verificaPermissoes');
    Route::resource('usuario','UsuarioController');

    //Rotas de Pessoas
    Route::get('pessoa/desativa','PessoaController@desativaPessoa');
    Route::post('pessoa/altera','PessoaController@updatePessoa');    
    Route::get('pessoa/especifico', 'PessoaController@retornaEspecifico');
    Route::resource('pessoa','PessoaController');

    //Rota de Processos
    
    Route::get('processo/ativos','ProcessoController@retornaProcessos');
    Route::resource('processo','ProcessoController');
    
    
    //Rota de Ações
    Route::resource('acao','AcaoController');
});
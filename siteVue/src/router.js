import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login'
import Alunos from './views/Alunos'
import Professores from './views/Professores'
import Clientes from './views/Clientes'
import Processos from './views/Processos'
import Acoes from './views/Acoes'
import Administrador from './views/Administrador'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path:'/home',
      name:'home',
      component: Home
    },
    {
      path:'/alunos',
      name:'alunos',
      component: Alunos
    },
    {
      path:'/professores',
      name:'professores',
      component: Professores
    },
    {
      path:"/clientes",
      name:'clientes',
      component: Clientes
    },
    {
      path:"/processos",
      name:'processos',
      component: Processos
    },
    {
      path:'/acoes',
      name:'acoes',
      component: Acoes
    },
    {
      path:'/administrador',
      name:'administrador',
      component: Administrador
    }
  ]
})

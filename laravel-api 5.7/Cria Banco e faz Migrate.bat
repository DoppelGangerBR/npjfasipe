@Echo off
Echo Criando usuario e banco, por favor aguarde 30 segundos
timeout /T 5
cd C:\xampp\mysql\bin\
Echo Criando banco de dados
mysql -u root -e "CREATE DATABASE npjfasipe CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;"
timeout /T 5
Echo Criando usuario
mysql -u root -e "CREATE USER 'npj'@'localhost' IDENTIFIED BY 'Fasipe@16';"
timeout /T 5
Echo Dando permissoes ao usuario
mysql -u root -e "GRANT ALL PRIVILEGES ON * . * TO 'npj'@'localhost';"
timeout /T 5
Echo Salvando permissoes
mysql -u root -e "FLUSH PRIVILEGES"
Echo Base e usuario criados!
Echo Iniciando o migrate
cd %~dp0
php artisan migrate
timeout /T 5
Echo Banco, usuario e tabelas criadas com sucesso!
Echo Consulte o arquivo de Migrate da tabela Usuarios para ver a senha de Administrador padrao
pause
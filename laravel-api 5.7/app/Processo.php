<?php

namespace App;
use App\ProcessoXPartes;
use Illuminate\Database\Eloquent\Model;

class Processo extends Model
{
    protected $fillable = ['numeroprocesso','codaluno','codprofessor','codacao','status','observacao'];
    protected $datas = ['created_at','updated_at','deleted_at'];
    protected $table = 'processos';

    public function Processo(){
        return $this->hasMany('App\Processo');
    }
    public function professor(){
        return $this->hasOne('App\Pessoa','id','codprofessor');
    }
    public function aluno(){
        return $this->hasOne('App\Pessoa','id','codaluno');
    }
    public function acao(){
        return $this->hasOne('App\Acao','id','codacao');
    }
    public function partesxprocesso(){
        return $this->hasMany('App\ProcessoXPartes','codprocesso','id');        
    }
    
}

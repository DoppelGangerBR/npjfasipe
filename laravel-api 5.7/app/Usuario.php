<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
class Usuario extends Authenticatable implements JWTSubject
{
    protected $fillable = ['usuario','senha','email','auth_token','ativo','administrador','podealterar','podeincluir','podeexcluir'];
    protected $datas = ['created_at','updated_at'];
    protected $table = 'usuarios';

    function Usuario(){
        return $this->hasMany('App\Usuario');
    }
    public function getJWTIdentifier(){
        return $this->getKey();
    }
    public function getJWTCustomClaims(){
        return [];
    }
}

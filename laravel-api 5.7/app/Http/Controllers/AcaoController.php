<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Acao;
class AcaoController extends Controller
{
    public function index(){
        $acoes = Acao::where('status','=',1)->get();
        return response()->json($acoes);
    }
    public function store(Request $request){
        $dados = $request->all();
        try{
            $acao = Acao::create($dados);
            if($acao){
                return response()->json(['status'=>true, $acao, 'msg'=>'Ação cadastrada com sucesso!']);
            }else{
                return response()->json(['status'=>false, 'msg'=>'Falha ao cadastrar ação, por favor, tente novamente!']);
            }
        }catch(\SqlException $e){
            return response()->json(['status'=>false, 'erro'=>$e, 'msg'=>'Falha ao cadastrar ação, por favor, chame o setor de desenvolvimento!']);
        }
    }
    public function destroy($id){
        try{
            $acao = Acao::where('id','=',$id)->update(['status'=>0]);
            if($acao){
                return response()->json(['status'=>true,'msg'=>'Ação excluida com sucesso!']);
            }else{
                return response()->json(['status'=>false,'msg'=>'Falha ao excluir ação, por favor, tente novamente!']);
            }
        }catch(\SqlException $e){
            return response()->json(['status'=>false,'msg'=>'Falha ao excluir ação, por favor, chame o setor de desenvolvimento!', 'erro'=>$e]);
        }
    }
    public function update($id,Request $request){
        $dados = $request->all();
        try{
            $acao = Acao::where('id','=',$id)->update($dados);
            if($acao){
                return response()->json(['status'=>true, 'msg'=>'Ação atualizada com sucesso!']);
            }else{
                return response()->json(['status'=>false, 'msg'=>'Falha ao atualizar ação, por favor, tente novamente!']);
            }
        }catch(\SqlException $e){
            return response()->json(['status'=>false,'msg'=>'Falha ao atualizar ação, por favor, chame o setor de desenvolvimento!', 'erro'=>$e]);
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
class UsuarioController extends Controller
{
    public function index(){
        $usuario = Usuario::where('ativo','=',1)->get();
        return response()->json($usuario);
    }
    public function store(Request $request){
        $dados = $request->all();
        $dados['senha'] = \Hash::make($dados['senha']);
        try{
            $novoUsuario = Usuario::create($dados);
            if($novoUsuario){
                return response()->json([$novoUsuario,'Status'=>true,'msg'=>'Usuário cadastrado com sucesso!']);
            }else{
                return response()->json(['Status'=>false,'msg'=>'Falha ao cadastrar novo usuário, por favor, tente novamente!']);
            }
        }catch(\Exception $e){
            return response()->json(['status'=>false,'erro'=>$e,'msg'=>'Falha ao cadastrar novo usuário, verifique o Console do Navegador!']);
        }
    }
    public function update(Request $request, $id){
        $dados = $request->all();
        $dados['senha'] = \Hash::make($dados['senha']);
        try{
            $usuarioAlterado = Usuario::where('id','=',$id)->update($dados);
            if($usuarioAlterado){
                return response()->json(['Status'=>true,'msg'=>'Usuário alterado com sucesso!']);
            }else{
                return response()->json(['Status'=>false,'msg'=>'Falha ao alterar usuário, tente novamente mais tarde!']);
            }
        }catch(\SqlException $e){
            return response()->json(['Status'=>false,'msg'=>'Falha ao alterar usuário, por favor, entre em contato com o setor de desenvolvimento','erro'=>$e]);
        }
    }
    public function destroy($id){
        try{
            $usuarioDeletado = Usuario::where('id','=',$id)->update(['ativo'=>0]);
            if($usuarioDeletado){
                return response()->json(['Status'=>true,'msg'=>'Usuário desativado com sucesso!']);
            }else{
                return response()->json(['Status'=>false,'msg'=>'Falha ao desativar o usuário!']);
            }
        }catch(\SqlException $e){
            return response()->json(['Status'=>false,'msg'=>'Falha ao desativar usuário, por favor, entre em contato com o setor de desenvolvimento','erro'=>$e]);
        }
    }
    public function verificaPermissoes(Request $request){
        $dados = $request->all();
        try{
            $permissoes = Usuario::select(['podealterar','podeincluir','podeexcluir'])->where('auth_token','=',$dados)->get();
            $podealterar = $permissoes[0]->podealterar;
            $podeincluir = $permissoes[0]->podeincluir;
            $podeexcluir = $permissoes[0]->podeexcluir;
            if($permissoes){
                return response()->json(['alterar'=>$podealterar, 'incluir'=>$podeincluir, 'excluir'=>$podeexcluir]);
            }
            return response()->json(['alterar'=>0, 'incluir'=>0]);
        }catch(\SqlException $e){
            return response()->json(['exception'=>$e]);
        }
    }
}
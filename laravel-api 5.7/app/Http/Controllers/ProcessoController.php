<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Processo;
use App\ProcessoXPartes;
use Illuminate\Support\Facades\DB;
class ProcessoController extends Controller
{
    public function index(){
        $processo = Processo::with(['professor','aluno','acao','partesxprocesso.partes'])->where('status','!=', 0)->orderBy('id','asc')->get();
        
        return response()->json($processo);
    }
    public function store(Request $request){
        $dados = $request->except('partes');
        $partes = $request->partes;
        try{
            $processo = Processo::create($dados);
            if($processo){
                $idPartes = count($partes);
                for($i = 0; $i < $idPartes; $i++){
                    DB::table('processoxparte')->insert(['codprocesso' => $processo->id, 'codparte' => $partes[$i]]);
                }
                return response()->json(["msg"=>"Processo cadastrado com sucesso!","status"=>true]);
            }else{
                return response()->json(['status'=>false]);
            }
        }catch(\SqlException $e){
            return response()->jsn(['erro'=>$e,'status'=>false]);
        }
    }
    public function retornaProcessos(Request $request){
        $tipo = $request->header('tipoProcesso');
        $processos = Processo::where('status','=',$tipo)->count();
        return response()->json($processos);
    }

    public function update(Request $request, $id){
        $dadosAlteracao = $request->except('partes');
        $partes = $request->partes;
        try{
            $processo = Processo::where('id','=',$id)->update($dadosAlteracao);
            if($processo){
                $qtdPartes = count($partes);
                DB::table('processoxparte')->where('codprocesso', $id)->delete();
                for($i = 0; $i < $qtdPartes; $i++){
                    DB::table('processoxparte')->insert(['codprocesso' => $id, 'codparte' => $partes[$i]]);
                }
                return response()->json(["msg"=>"Processo alterado com sucesso!","status"=>true]);
            }else{
                return response()->json(["msg"=>"Falha ao alterar o processo, por favor, tente mais tarde!","status"=>false]);
            }
        }catch(\SqlException $e){
            return response()->json(['status'=>false,'erro'=>$e, 'msg'=>'Falha ao alterar o processo, por favor, entre em contato com o setor de desenvolvimento!']);
        }
    }

    public function destroy($id){
        try{
            $processo = Processo::where('id','=',$id)->update(['status'=>0]);
            if($processo){
                return response()->json(['status'=>true,'msg'=>'Processo excluido com sucesso!']);
            }else{
                return response()->json(['status'=>false,'msg'=>'Não foi possivel excluir o processo, por favor, tente mais tarde']);
            }
        }catch(\SqlException $e){
            return response()->json(['status'=>false,'msg'=>'Falha ao excluir processo, por favor, entre em contato o setor de desenvolvimento!', 'erro'=>$e]);
        }
        
    }
}

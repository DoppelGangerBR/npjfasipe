<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Processo;
class ProcessoXPartes extends Model
{
    protected $fillable = ['codparte','codprocesso'];
    protected $hidden = ['created_at','updated_at','codparte','codprocesso','id'];
    protected $table = 'processoxparte';


    public function partes(){
        //return $this->belongsTo('App\Pessoa','codparte','id');
        return $this->belongsTo('App\Pessoa','codparte');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pessoa extends Model
{
    protected $fillable = ['nome','telefone', 'cpf', 'rg', 'email', 'tipopessoa', 'status', 'ra', 'idade','celular'];
    protected $datas = ['created_at','updated_at','deleted_at'];
    protected $table = 'pessoas';

    public function Pessoas(){
        return $this->hasMany('App\Pessoas');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pessoas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pessoas', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('nome',100)->unique();
            $table->string('telefone',20)->nullable();
            $table->string('celular',20)->nullable();
            $table->string('cpf',20)->nullable();
            $table->string('rg',10)->nullable();
            $table->string('email',100)->nullable();
            $table->smallInteger('tipopessoa'); //{1 = Aluno} {2 = Professor} {3 = Cliente}
            $table->smallInteger('status')->default(1); //{1 = ativo} {2 = desativado}
            $table->timestamps();
            //Criar manualmente depois os campos RA e idade
            //Especificar o tamanho máximo da coluna do RA para o tamanho maximo do numero do RA
            //Caso contrario, vai dar erro
            
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pessoas');
    }
}
